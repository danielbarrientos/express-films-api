const jwt = require('jwt-simple')
const moment = require('moment')

const checkToken = (req,res, next)=> {

  
    if(! req.headers['x-token']){
        return res.json({
            error: 'x-token is required in headers'
        })
    }

    const token = req.headers['x-token'];
    let payload = {
        
    }
    try {
        payload = jwt.decode(token,'secret_key')

        if(payload.expiredAt < moment.unix()){
            return res.json({
                error: 'token expired'
            })
        }

        req.userId = payload.userId
    } catch (error) {
        return res.json({error: 'invalid token'})
    }

    next();
}

module.exports = {
    checkToken: checkToken
}