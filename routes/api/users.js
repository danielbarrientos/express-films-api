const router = require('express').Router()
const bcrypt = require('bcryptjs')
const { User } = require('../../db')
const { check, validationResult } = require('express-validator') 
const moment = require('moment')
const jwt = require('jwt-simple')

router.post('/register',[
    check('username','The name is required').not().isEmpty(),
    check('password','The password is required').not().isEmpty(),
    check('email', 'The email is invalid').isEmail()
], async(req,res)=>{

    const errors = validationResult(req)

    if(! errors.isEmpty()){
        return res.status(422).json({
            errors: errors.array()
        })
    }

    req.body.password = bcrypt.hashSync(req.body.password,10)
    const user = await User.create(req.body);
     res.json({
         ok:true,
         msg:"created success",
         data: user
     });
})

router.post('/login', async (req,res) => {

    const user =  await User.findOne({where: {email: req.body.email}})
    if(user){
        const  same = bcrypt.compareSync(req.body.password,user.password);
        if(same){
            res.json({
                ok: true,
                data:user,
                token: createToken(user)
            })
        }
        else{
            res.json({error: 'invalid credentials'})
        }
    }
    else{
        res.json({error: 'invalid credentials'})
    }
})

const createToken = (user)=> {
    const payload = {
        userId: user.id,
        createdAt: moment().unix(),
        expiredAt: moment().add(5,'minutes').unix()
    }

    return jwt.encode(payload,'secret_key')
}
module.exports = router