const express = require('express')
const bodyParser = require('body-parser')

const apiRouter = require('./routes/api.js')

const app = express()

require('./db')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: true}))


/**Routes */
app.use('/api', apiRouter)

app.listen(4000, () => {
    console.log(`Server started on port 4000`);
});
